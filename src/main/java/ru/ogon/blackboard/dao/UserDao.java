package ru.ogon.blackboard.dao;

import ru.ogon.blackboard.model.User;

public interface UserDao {
	
    User findById(int id);
    
    User findBySSO(String sso);
}
