package ru.ogon.blackboard.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ru.ogon.blackboard.model.Comment;

@Repository("commentDao")
public class CommentDaoImpl extends AbstractDao<Integer, Comment> implements CommentDao {

	@Override
	public Comment findById(int id) {
		return getByKey(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> findByPostId(int postId) {
		return (List<Comment>) createEntityCriteria().add(Restrictions.eq("post.id", postId)).list();
	}

}
