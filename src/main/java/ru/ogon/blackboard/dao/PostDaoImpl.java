package ru.ogon.blackboard.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ru.ogon.blackboard.model.Post;

@Repository("postDao")
public class PostDaoImpl extends AbstractDao<Integer, Post> implements PostDao {
	
	@Override
	public Post findById(int id) {
		return getByKey(id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findByBoardCaption(String boardCaption) {
		return (List<Post>) createEntityCriteria().add(Restrictions.eq("board.caption", boardCaption)).list();
	}
}
