package ru.ogon.blackboard.dao;

import java.util.List;

import ru.ogon.blackboard.model.Post;

public interface PostDao {
	
	Post findById(int id);
	List<Post> findByBoardCaption(String boardCaption);
	void persist(Post post);
	void update(Post post);
	void delete(Post post);
}
