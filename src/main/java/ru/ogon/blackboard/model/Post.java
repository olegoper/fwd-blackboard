package ru.ogon.blackboard.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="posts")
public class Post {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
	
	@Column(name = "created_at", columnDefinition = "timestamp", nullable = false)
    private LocalDateTime createdAt;
	
	@Column(name="caption", nullable=false)
	private String caption;
	
	@Column(name="text", nullable=false)
	private String text;
	
	@ManyToOne
    @JoinColumn(name = "post_id", nullable=false)
    private Board board;
	
	@ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
	
	@OneToMany(mappedBy="post", fetch=FetchType.EAGER)
    private Set<Comment> comments;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
}
