package ru.ogon.blackboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.ogon.blackboard.dao.PostDao;
import ru.ogon.blackboard.model.Post;

@Service("postService")
@Transactional
public class PostServiceImpl implements PostService {
	
	@Autowired
    private PostDao dao;

	@Override
	public Post findById(int id) {
		return dao.findById(id);
	}

	@Override
	public List<Post> findByBoardCaption(String boardCaption) {
		return dao.findByBoardCaption(boardCaption);
	}

	@Override
	public void persist(Post post) {
		dao.persist(post);		
	}

	@Override
	public void update(Post post) {
		dao.update(post);
	}

	@Override
	public void delete(Post post) {
		dao.delete(post);
	}
}
