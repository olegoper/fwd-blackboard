package ru.ogon.blackboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.ogon.blackboard.dao.BoardDao;
import ru.ogon.blackboard.model.Board;

@Service("boardService")
@Transactional
public class BoardServiceImpl implements BoardService {

	@Autowired
    private BoardDao dao;
	
	@Override
	public Board findById(int id) {
		return dao.findById(id);
	}

	@Override
	public List<Board> findAll() {
		return dao.findAll();
	}

	@Override
	public void persist(Board board) {
		dao.persist(board);
	}

	@Override
	public void update(Board board) {
		dao.update(board);
	}

	@Override
	public void delete(Board board) {
		dao.delete(board);
	}
}
