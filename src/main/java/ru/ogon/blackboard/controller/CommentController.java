package ru.ogon.blackboard.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ru.ogon.blackboard.configuration.Routes;
import ru.ogon.blackboard.model.Comment;
import ru.ogon.blackboard.model.CustomUserDetails;
import ru.ogon.blackboard.service.CommentService;
import ru.ogon.blackboard.service.PostService;

@Controller
public class CommentController {
	
	@Autowired
    private CommentService commentService;
	
	@Autowired
    private PostService postService;
	
	@RequestMapping(value = Routes.comment_details + "{id}", method = RequestMethod.GET)
    public String commentView(ModelMap model, @PathVariable(value="id") int id) {
		model.addAttribute("routes", Routes.getRoutes());
        model.addAttribute("comment", commentService.findById(id));
        return "comment_view";
    }
	
	@RequestMapping(value = Routes.comment_new, method = RequestMethod.GET)
    public String newPostGet(ModelMap model, @RequestParam(value="postId") int postId) {
		model.addAttribute("routes", Routes.getRoutes());
		model.addAttribute("postId", postId);
        return "comment_create_form";
    }
	
	@RequestMapping(value = Routes.comment_new, method = RequestMethod.POST)
	@Transactional
    public String newPostPost(ModelMap model
    		, @AuthenticationPrincipal CustomUserDetails user
    		, @RequestParam("caption") String caption
    		, @RequestParam("text") String text
    		, @RequestParam("postId") int postId) {
		model.addAttribute("routes", Routes.getRoutes());
		Comment comment = new Comment();
		comment.setPost(postService.findById(postId));
		if (user != null) {
			comment.setUser(user.getDomainModel());
		}
		comment.setCaption(caption);
		comment.setText(text);
		comment.setCreatedAt(LocalDateTime.now());
		commentService.persist(comment);
        return "redirect:" + Routes.post_details + postId;
    }
}
