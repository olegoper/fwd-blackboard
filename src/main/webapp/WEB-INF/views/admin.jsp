<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:base>
  <jsp:attribute name="title">Admin page</jsp:attribute>
      
  <jsp:body>
  	<p>Dear <strong>${user}</strong>, Welcome to Admin Page.</p>
    <p><a href="${routes.logout}">Logout</a></p>
  </jsp:body>
</t:base>
