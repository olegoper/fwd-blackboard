<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:base>
  <jsp:attribute name="title">Boards</jsp:attribute>
      
  <jsp:body>
  	<h1>Boards</h1>
    <a class="btn btn-primary" href="${routes.board_new}">New Board</a>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
 		  <td>Caption</td>
 		  <td>Posts</td>
 		  <td>About</td>
 		</tr>
      </thead>
      <c:forEach items="${boardList}" var="board">
      <tr>
     	<td><a href="${routes.board_details}${board.id}"><c:out value="${board.caption}"/></a></td>
     	<td><c:out value="${fn:length(board.posts)}"/></td>
     	<td><c:out value="${board.description}"/></td>
      </tr>
      </c:forEach>
    </table>
  </jsp:body>
</t:base>
