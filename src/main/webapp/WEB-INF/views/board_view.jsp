<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:base>
  <jsp:attribute name="title">Board <c:out value="${board.caption}" /></jsp:attribute>
      
  <jsp:body>
  	<h1>${board.caption}</h1>
  	<a class="btn btn-primary" href="${routes.board}">Back</a>
    <a class="btn btn-primary" href="${routes.post_new}?boardId=${board.id}">New Post</a>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
 		  <td>Post</td>
 		  <td>Author</td>
 		  <td>Comments</td>
 		  <td>Content</td>
 		</tr>
      </thead>
      <c:forEach items="${board.posts}" var="post">
      <tr>
     	<td><a href="${routes.post_details}${post.id}"><c:out value="${post.caption}"/></a></td>
     	<td><c:out value="${post.user.ssoId}"/></td>
     	<td><c:out value="${fn:length(post.comments)}"/></td>
     	<td><c:out value="${post.text}"/></td>
      </tr>
      </c:forEach>
    </table>
  </jsp:body>
</t:base>
