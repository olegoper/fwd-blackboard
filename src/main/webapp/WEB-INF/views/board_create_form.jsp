<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:base>
  <jsp:attribute name="title">New board</jsp:attribute>
      
  <jsp:body>
	<h1>New Board</h1>
	<form:form action="${routes.board_new}" method="POST">
	<label class="control-label col-sm-2">Board Name:</label>
	<input class="form-control" type="text" name="caption">
	<br/>
	<label class="control-label col-sm-2">Description:</label>
	<textarea class="form-control" name="description"></textarea>
	<br/>
	<input class="btn btn-success" type="submit" value="Create" />
	<a class="btn btn-danger" href="${routes.board}">Cancel</a>
    </form:form>
  </jsp:body>
</t:base>